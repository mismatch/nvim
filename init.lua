-- load the plugins first in order to make 'PackerInstall' available
-- plugin settings are below
require('settings')
require('plugins')

require('mappings')
require('statusline')

-- plugins
-- general settings for plugins are in /lua/plugins/init.lua
require('plugins.gitsigns')
require('plugins.nvim-autopairs')
require('plugins.nvim-colorizer')
require('plugins.comment')
require('plugins.nvim-cmp')
require('plugins.nvim-neogit')
require('plugins.nvim-notify')
require('plugins.nvim-web-devicons')
require('plugins.nvim-tree')
require('plugins.telescope')
require('plugins.telescope-file-browser')
require('plugins.treesitter')
require('plugins.vimtex')
require('plugins.vim-markdown')
require('plugins.vim-vsnip')
require('plugins.symbols-outline')

-- lsp
-- general settings for nvim-lspconfig are in /lua/lsp/init.lua
require('lsp')
require('lsp.bash')
require('lsp.css')
require('lsp.denols')
require('lsp.emmet_ls')
require('lsp.html')
require('lsp.json')
require('lsp.lua')
require('lsp.r')
require('lsp.pyright')
require('lsp.texlab')
require('lsp.tsserver')
require('lsp.yml')

-- colour scheme settings in lua/colours/init.lua
-- load other plugins first
-- settings for each colour scheme are in lua/colours
require('colours')

-- settings for gui apps, eg neovide
require('gui')
