-- requires nvim-web-devicons for filetype icon
-- requires nvim-signify for git status

local fn = vim.fn
local api = vim.api

local M = {}

-- highlight groups - ready to add to colourschemes
M.colours = {
  active       = "%#StatusLine#",
  inactive     = "%#StatusLineNC#",
  mode         = "%#StatusLine#",
  git          = "%#StatusLine#",
  filetype     = "%#StatusLine#",
  line_col     = "%#StatusLine#",
  percentage   = "%#StatusLine#",
  modified     = "%#StatusLine#",
  filename     = "%#StatusLine#",
}

M.trunc_width = setmetatable({
  git_status = 90,
  filename = 140,
}, {
  __index = function()
    return 80
  end,
})

M.is_truncated = function(_, width)
  local current_width = api.nvim_win_get_width(0)
  return current_width < width
end

M.modes = setmetatable({
  ["n"]  = "N",
  ["no"] = "N·P",
  ["v"]  = "V",
  ["V"]  = "V·L",
  [""] = "V·B", -- this is not ^V, but it's , they're different
  ["s"]  = "S",
  ["S"]  = "S·L",
  [""] = "S·B", -- same with this one, it's not ^S but it's 
  ["i"]  = "I",
  ["ic"] = "I",
  ["R"]  = "R",
  ["Rv"] = "V·R",
  ["c"]  = "C",
  ["cv"] = "V·E",
  ["ce"] = "E",
  ["r"]  = "P",
  ["rm"] = "M",
  ["r?"] = "C",
  ["!"]  = "S",
  ["t"]  = "T",
}, {
  __index = function()
    return "U" -- handle edge cases
  end,
})

M.get_current_mode = function(self)
  local current_mode = api.nvim_get_mode().mode
  return string.format(" %s ", self.modes[current_mode]):lower()
end

M.get_git_status = function(self)
  -- use fallback because it doesn't set this variable on the initial `BufEnter`
  local signs = vim.b.gitsigns_status_dict
    or { head = "", added = 0, changed = 0, removed = 0 }
  local is_head_empty = signs.head ~= ""

  self:is_truncated(self.trunc_width.git_status)
    return is_head_empty and string.format("  %s", signs.head or "") or ""
end

M.get_filepath = function(self)
  local filepath = fn.fnamemodify(fn.expand "%", ":.:h")
  if
    filepath == ""
    or filepath == "."
    or self:is_truncated(self.trunc_width.filename)
  then
    return ""
  end

  return string.format("%%<%s/", filepath)
end

M.get_filename = function()
  local filename = fn.expand "%:t"
  if filename == "" then
    return ""
  end
  return filename
end

M.get_filetype = function()
  local file_name, file_ext = fn.expand "%:t", fn.expand "%:e"
  local icon = require("nvim-web-devicons").get_icon(
    file_name,
    file_ext,
    { default = true }
  )
  local filetype = vim.bo.filetype

  if filetype == "" then
    return "🔾 "
  end
  return string.format("%s", icon):lower()
end

M.get_line_col = function()
  return " %3l %3c "
end

M.get_percentage = function()
  return " %P"
end

M.get_modified = function()
  return " %M"
end

M.set_active = function(self)
  local colours = self.colours

  local mode = colours.mode .. self:get_current_mode()
  local git = colours.git .. self:get_git_status()

  local filename = string.format(
    " %s%s%s%s",
    colours.inactive,
    self:get_filepath(),
    colours.filename,
    self:get_filename()
  )

  local filetype = colours.filetype .. self:get_filetype()
  local line_col = colours.line_col .. self:get_line_col()
  local percentage = colours.percentage .. self:get_percentage()
  local modified = colours.modified .. self:get_modified()

  return table.concat {
    colours.active,
    filetype,
    git,
    filename,
    modified,
    "%=",
    mode,
    line_col,
    percentage
  }
end

M.set_inactive = function(self)
  return self.colours.inactive .. "%= %f %="
end

-- return M using Statusline metatable
Statusline = setmetatable(M, {
  __call = function(self, linemode)
    return self["set_" .. linemode](self)
  end,
})

-- set statusline
-- TODO: replace this once we can define autocmd using lua
vim.cmd [[
  augroup Statusline
  au!
  au WinEnter,BufEnter * setlocal statusline=%!v:lua.Statusline('active')
  au WinLeave,BufLeave * setlocal statusline=%!v:lua.Statusline('inactive')
  augroup END
]]
