local cmd = vim.cmd

cmd('let g:vsnip_snippet_dir = expand("~/.config/nvim/snippets/")')
