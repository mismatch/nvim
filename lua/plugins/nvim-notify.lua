require("notify").setup({
  -- animation style (see below for details)
  stages = "fade_in_slide_out",

  -- function called when a new window is opened, use for changing win settings/config
  on_open = nil,

  -- function called when a window is closed
  on_close = nil,

  -- render function for notifications. See notify-render()
  render = "default",

  -- default timeout for notifications
  timeout = 2000,

  -- for stages that change opacity this is treated as the highlight behind the window
  -- set this to either a highlight group or an RGB hex value e.g. "#000000"
  background_colour = "#123213",

  -- minimum width for notification windows
  minimum_width = 50,

  -- icons for the different levels
  icons = {
    ERROR = " ",
    WARN = " ",
    INFO = " ",
    DEBUG = " ",
    TRACE = "✎ ",
  },
})

-- allow notifications using this plugin
vim.notify = require('notify')

-- load the notify extension for telescope to enable :Telescope notify<cr>
require("telescope").load_extension("notify")
