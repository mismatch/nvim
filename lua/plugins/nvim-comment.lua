require('nvim_comment').setup({
  -- linters prefer comment and line to have a space in between markers
  marker_padding = true,

  -- should comment out empty or whitespace only lines
  comment_empty = true,

  -- should key mappings be created
  create_mappings = true,

  -- normal mode mapping left hand side
  line_mapping = "gcc",

  -- visual/operator mapping left hand side
  operator_mapping = "gc"
})
