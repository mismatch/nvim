-- each plugin is configured in the nvim/plugins/<plugin>.lua

-- define local variables
local execute = vim.api.nvim_command
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

-- check packer.nvim is installed and install if necessary
if fn.empty(fn.glob(install_path)) > 0 then
    execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
    execute "packadd packer.vim"
end

return require('packer').startup({function(use)
    -- packer can manage itself
    use 'wbthomason/packer.nvim'

    -- treesitter
    use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}

    -- telescope
    use {
        'nvim-telescope/telescope.nvim', requires = {{'nvim-lua/plenary.nvim'}}
    }

    -- telescope file_browser extension
    use { "nvim-telescope/telescope-file-browser.nvim" }

    -- look into how to use the following:
    -- use 'nvim-telescope/telescope-fzy-native.nvim'

    -- devicons
    use 'kyazdani42/nvim-web-devicons'

    -- settings for configuring lsp clients
    use 'neovim/nvim-lspconfig'

    -- completion menu
    use {
        "hrsh7th/nvim-cmp",
        requires = {
            "L3MON4D3/LuaSnip",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-calc",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lua",
            "hrsh7th/cmp-emoji",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            "f3fora/cmp-spell",
            "onsails/lspkind-nvim",
            "kdheepak/cmp-latex-symbols",
            -- "saadparwaiz1/cmp_luasnip"
        }
    }

    -- colour schemes
    use 'sainnhe/sonokai'
    use 'sainnhe/everforest'
    use 'shaunsingh/nord.nvim'
    use 'folke/tokyonight.nvim'
    use 'marko-cerovac/material.nvim'


    -- git
    use 'lewis6991/gitsigns.nvim'
    use 'TimUntersberger/neogit'

    -- automatically close brackets
    use 'windwp/nvim-autopairs'

    -- keybindings for comments
    -- use 'terrortylor/nvim-comment'

    use 'numToStr/Comment.nvim'

    -- preview colours
    use 'norcalli/nvim-colorizer.lua'

    -- latex settings
    use 'lervag/vimtex'

    -- markdown
    use 'plasticboy/vim-markdown'

    -- notify
    use 'rcarriga/nvim-notify'

    -- colour picker
    use 'uga-rosa/ccc.nvim'

    -- document outlines

    use 'simrat39/symbols-outline.nvim'

    use {
  'nvim-tree/nvim-tree.lua',
  requires = {
    'nvim-tree/nvim-web-devicons', -- optional, for file icons
  },
  tag = 'nightly' -- optional, updated every week. (see issue #1193)
}

end,
config = {
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'none' })
    end
  }
}}
)
