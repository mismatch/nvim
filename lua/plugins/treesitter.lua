require('nvim-treesitter.configs').setup {
  ensure_installed = {"html", "latex", "python", "lua", "javascript", "css", "r", "comment", "json", "c", "yaml", "bash", "typescript"}, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ignore_install = { "haskell" }, -- list of parsers to ignore installing
  highlight = {
    enable = true,              -- false will disable the whole extension
    -- disable = {"latex"},        -- lsp functions currently break treesitter
  },
  indentation = {
    enable = false
  },
}

-- these settings should enable folding, currently doesn't work
vim.wo.foldmethod = 'expr'
vim.wo.foldexpr = 'nvim_treesitter#foldexpr()'
