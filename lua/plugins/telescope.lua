local map = vim.keymap.set
local opts = { noremap = true, silent = true }

local M = {}

function M.nvim_dots()
  require('telescope.builtin').find_files{
    shorten_path = false,
    cwd = '~/.config/nvim',
    prompt_title = 'nvim dots',
  }
end

function M.dots()
  require('telescope.builtin').find_files{
    shorten_path = false,
    cwd = '~/.config',
    prompt_title = 'dots',
  }
end

-- use <esc> to quit directly from insert mode
-- the default is to enter normal mode
local actions = require('telescope.actions')
require('telescope').setup{
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = actions.close
      },
    },
  }
}

map('n', '<leader>f',           ':Telescope file_browser<cr>', opts)
map('n', '<leader><leader>',    ':Telescope<cr>', opts)
map('n', '<leader>ff',          ':Telescope find_files<cr>', opts)
-- map('n', '<leader>fb',          ':Telescope buffers<cr>', opts)
map('n', '<leader>fh',          ':Telescope oldfiles<cr>', opts)
map('n', '<leader>fg',          ':Telescope git_files<cr>', opts)
map('n', '<leader>fi',          ':Telescope help_tags<cr>', opts)
map('n', '<leader>fr',          ':Telescope live_grep<cr>', opts)
map('n', '<leader>fm',          ':Telescope marks<cr>', opts)
map('n', '<leader>fc',          ':Telescope git_commits<cr>', opts)
map('n', '<leader>fl',          ':Telescope current_buffer_fuzzy_find<cr>', opts)


-- custom telescope prompts
map('n', '<leader>fv',          ':lua require("plugins.telescope").nvim_dots()<cr>', opts)
map('n', '<leader>fd',          ':lua require("plugins.telescope").dots()<cr>', opts)

return M
