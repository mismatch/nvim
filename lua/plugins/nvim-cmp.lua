local cmp = require'cmp'
local lspkind = require('lspkind')
local check_back_space = function()
    local col = vim.fn.col '.' - 1
    return col == 0 or vim.fn.getline('.'):sub(col, col):match '%s' ~= nil
end
local luasnip = require("luasnip")

local t = function(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

cmp.setup({
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end
    },
    mapping = {
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                vim.fn.feedkeys(t("<Plug>luasnip-expand-or-jump"), "")
            elseif check_back_space() then
                vim.fn.feedkeys(t("<Tab>"), "n")
            else
                fallback()
            end
        end, {
        "i",
        "s",
    }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                vim.fn.feedkeys(t("<Plug>luasnip-jump-prev"), "")
            else
                fallback()
            end
        end, {
        "i",
        "s",
    }),
    ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
    ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({
        behavior = cmp.ConfirmBehavior.Replace,
        select = true,
    })
        },
    sources = {
        { name = 'nvim_lsp'},
        { name = 'buffer', keyword_length = 3},
        { name = 'path'},
        { name = 'luasnip'},
        { name = 'spell', keyword_length = 5},
        { name = 'calc'},
        { name = 'nvim_lsp_signature_help'},
        { name = 'nvim_lua'},
        -- { name = 'latex_symbols', priority = 1, max_item_count = 5},
        { name = 'emoji'},
    },
    formatting = {
        format = function(entry, vim_item)
            vim_item.kind = lspkind.presets.default[vim_item.kind]

            -- set a name for each source
            vim_item.menu = ({
                buffer = "buffer",
                nvim_lsp = "lsp",
                -- luasnip = "LuaSnip",
                nvim_lua = "lua",
                latex_symbols = "latex",
                spell = "spell",
            })[entry.source.name]

            return vim_item
        end
    }

})

