local map = vim.keymap.set
local opts = { noremap = true, silent = true }
local neogit = require('neogit')

neogit.setup {
    disable_commit_confirmation = true,
    mappings = {
        -- modify status buffer mappings
        status = {
            -- adds a mapping with "l" as key that does the "LogPopup" command
            ["l"] = "LogPopup",
        }
    },
    signs = {
        -- { CLOSED, OPENED }
        section = { "▸", "▾" },
        item = { "▸", "▾" },
        hunk = { "", "" },
    },
}

map('n','<leader>gg', '<cmd>Neogit<cr>', opts)
