local map = vim.keymap.set
local opts = { noremap = true, silent = true }
require("symbols-outline").setup()
map('n', '<leader>so', ':SymbolsOutline<cr>', opts)
