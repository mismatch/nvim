-- npm install -g vscode-css-languageserver-bin

-- enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

require'lspconfig'.cssls.setup {
  capabilities = capabilities,
}
