-- from https://github.com/neovim/nvim-lspconfig

local map = vim.keymap.set
local opts = { noremap = true, silent = true }

map('n','gd','<cmd>lua vim.lsp.buf.definition()<cr>', opts)
map('n','gD','<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
map('n','gr','<cmd>lua vim.lsp.buf.references()<cr>', opts)
map('n','K','<cmd>lua vim.lsp.buf.hover()<cr>', opts)
map('n','[d','<cmd>lua vim.diagnostic.goto_prev()<cr>', opts)
map('n',']d','<cmd>lua vim.diagnostic.goto_next()<cr>', opts)
map('n','gl','<cmd>lua vim.diagnostic.open_float()<cr>', opts)

-- diagnostic signs in the gutter
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

-- customise how diagnostics are displayed
vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = false,
  update_in_insert = false,
  severity_sort = false,
})
