local nvim_lsp = require("lspconfig")
require'lspconfig'.tsserver.setup{
  root_dir = nvim_lsp.util.root_pattern("package.json"),
}
