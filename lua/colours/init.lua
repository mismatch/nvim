require('colours.sonokai')
require('colours.everforest')
require('colours.nord')
require('colours.tokyonight')

-- set the required colorscheme here
local colorscheme = 'tokyonight'

-- check if the above colorscheme exists, and set it if available
local exists, _ = pcall(vim.cmd, 'colorscheme ' .. colorscheme)

-- send a notification if the scheme is unavailable
if not exists then
    vim.notify('the colorscheme ' .. colorscheme .. ' was not found\ndefault colorscheme used instead')
    return
end
