-- settings for the sonokai colour theme

-- signcolumn background
vim.cmd('let g:sonokai_sign_column_background = "none"')
vim.cmd('let g:sonokai_menu_selection_background = "red"')
