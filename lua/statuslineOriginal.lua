-- custom status line

-- define the bar
-- the numbers allow for a minimum amount of space
local stl = {
  '%f',
  '%=',
  ' %2M',
  ' %{&filetype}',
  ' %4l%4c',
  '  %P',
  ' %2{mode()}',
}

-- set the bar
vim.o.statusline = table.concat(stl)
