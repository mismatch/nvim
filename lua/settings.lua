local o   =  vim.o
local bo  =  vim.bo
local wo  =  vim.wo

-- no warnings when switching buffers
o.hidden = true

-- filename in the terminal
o.title  =  true

-- ignore case in searches and wildmenu
o.ignorecase = true

-- override ignorecase if pattern contains capitals
o.smartcase = true

-- numbers in the signcolumn
wo.number = false

-- expand the tab character to spaces
bo.expandtab = true
o.expandtab = true

-- number of spaces per tab
bo.tabstop = 4
o.tabstop = 4

-- number of spaces per tab
bo.shiftwidth = 4
o.shiftwidth = 4

-- number of spaces a tab represents
bo.softtabstop = 4
o.softtabstop = 4

-- always show the signcolumn (gutter)
wo.signcolumn = 'yes'

-- new horizontal splits are below the current window
o.splitbelow = true

-- new vertical splits are to the right of the current window
o.splitright = true

-- do not show typed commands
o.showcmd = false

-- set command line height to 0
-- o.cmdheight=0

-- show current mode
o.showmode = false

-- allow mouse in all modes
o.mouse = 'a'

-- spell check in en_gb
bo.spelllang = 'en_gb'

-- show at least 3 lines above / below cursor
o.scrolloff = 3

-- same as above but for columns
o.sidescrolloff = 3

-- highlight current line
wo.cursorline = true

-- share clipboard between applications
o.clipboard = 'unnamedplus'

-- set the height of popup menus
o.pumheight= 10

-- break lines on complete words rather than a set number of characters
wo.linebreak = true

-- listchars
local lschars = {
  'tab:▸ ,', -- space after the character is necessary
  'eol:¬,',
  'nbsp:⦸,',
  'extends:»,',
  'precedes:«,',
  'trail:•',
}

wo.listchars = table.concat(lschars)

-- fillchars
local fchars = {
  'eob: ,', -- suppress ~ on blank lines in endofbuffer
}
wo.fillchars = table.concat(fchars)

-- short messages
local sms = {
  'x',      -- 'unix' instead of 'unix format'
  'I',      -- supress splash screen
  'c',      -- no match n of n messages
  'm',      -- use + instead of 'modified'
  'F',      -- don't give file information
  'W',      -- don't give "written" when saving
}

o.shortmess = table.concat(sms)

-- disable folding
o.foldenable = false

o.encoding = 'utf-8'

-- set before the colour scheme
o.termguicolors = true
o.background = 'dark'

o.laststatus = 3

-- xclip gives an error, this overrides it to use xsel instead
-- multi-line dictionaries causes an error (even with \ on new lines)
-- see h: clipboard for more information
vim.cmd([[

let g:clipboard={'name': 'xsel_override','copy': {'+': 'xsel --input --clipboard','*': 'xsel --input --primary'},'paste': {'+': 'xsel --output --clipboard','*': 'xsel --output --primary',},'cache_enabled': 1}
]])


