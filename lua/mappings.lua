local map = vim.keymap.set
local opts = { noremap = true, silent = true }

-- set leader key to space
vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- alternative to escape
map('i', 'jk', '<esc>', opts)

-- window navigation
map('n', '<c-h>', '<c-w>h', opts)
map('n', '<c-j>', '<c-w>j', opts)
map('n', '<c-k>', '<c-w>k', opts)
map('n', '<c-l>', '<c-w>l', opts)
map('n', '<leader>ww', '<c-w>w', opts)
map('n', '<leader>w', '<c-w>w', opts)

-- close windows
map('n', '<leader>wc', ':close<cr>', opts)
map('n', '<leader>wd', ':close<cr>', opts)

-- quitting
map('n', '<leader>q', ':q<cr>', opts)

-- avoid entering command history mode when exiting
map('n', 'q:', ':q<cr>', opts)

-- tab switch buffer
map('n', '<tab>', ':bnext<cr>', opts)
map('n', '<s-tab>', ':bprevious<cr>', opts)

-- move by display lines
map('n', 'j', 'gj', opts)
map('n', 'k', 'gk', opts)

-- clear search highlights and clear the command line
map('n', '<esc><esc>', ':noh|call feedkeys(":","nx")<cr>', opts)

-- toggle spell check
map('n', '<leader>s', ':set spell!<cr>', opts)
map('n', '<leader>ss', ':set spell!<cr>', opts)

-- toggle list markers
map('n', '<leader>sl', ':set list!<cr>', opts)
map('n', '<leader>sn', ':set number! relativenumber!<cr>', opts)

-- toggle background colour
map('n', '<leader>sb', ':let &background = (&background == "dark"? "light" : "dark")<cr>', opts)

-- change working directory to path of the current file
map('n', '<leader>cd', ':cd %:p:h<cr>', opts)

-- change working directory to ~/.config
map('n', '<leader>cc', ':cd ~/.config<cr>', opts)

-- create a new buffer/file
map('n', '<leader>bn', ':enew<cr>', opts)

-- jump out of the current brackets
map('i', '<c-j>', "<esc>/[)}\"\'$\\]>]<cr>:nohl<cr>a", opts)

-- add undo break points at punctuation
map('i', ',',',<c-g>u', opts)
map('i', '.','.<c-g>u', opts)
map('i', '!','!<c-g>u', opts)
map('i', '?','?<c-g>u', opts)

-- yank to end of the line
-- make Y behave like D, C etc.
map('n', 'Y','y$', opts)

-- keep searches centered
map('n', 'n','nzzzv', opts)
map('n', 'N','Nzzzv', opts)

-- keep line concatenation centered
map('n', 'J','mzJ`z', opts)

-- moving text
map('v', 'J',":m '>+1<cr>gv=gv", opts)
map('v', 'K',":m '<-2<cr>gv=gv", opts)
map('n', '<leader>j',':m .+1<cr>==', opts)
map('n', '<leader>k',':m .-2<cr>==', opts)

-- terminal escape
map('t', '<esc>', '<c-\\><c-n>', opts)

-- enter a literal (verbatim) escape in terminal
map('t', '<c-v><esc>', '<esc>', opts)

-- load terminal and enter insert mode
map('n', '<leader>t', ':vsplit | vertical resize 50 | term<cr>i', opts)

-- switch windows in terminal
map('t', '<leader>ww', '<c-\\><c-n><c-w>w', opts)
